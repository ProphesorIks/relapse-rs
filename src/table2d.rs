pub struct Table2d<T>
where
    T: Copy + Clone,
{
    dimensions: (u32, u32),
    elements: Vec<T>,
}

impl<T> Table2d<T>
where
    T: Copy + Clone,
{
    pub fn new(x: u32, y: u32, default: T) -> Table2d<T> {
        Table2d {
            dimensions: (x, y),
            elements: vec![default; (x * y) as usize],
        }
    }

    pub fn set(&mut self, x: u32, y: u32, element: T) {
        if x >= self.dimensions.0 || y >= self.dimensions.1 {
            panic!("Out of bounds")
        }
        self.elements[((self.dimensions.0 * y) + x) as usize] = element;
    }

    pub fn get(&self, x: u32, y: u32) -> T {
        if x >= self.dimensions.0 || y >= self.dimensions.1 {
            panic!("Out of bounds")
        }
        self.elements[((self.dimensions.0 * y) + x) as usize]
    }

    pub fn _to_slice(&self) -> &[T] {
        self.elements.as_slice()
    }
}

#[cfg(test)]
mod tests {
    use std::default;

    use crate::table2d::Table2d;

    #[test]
    fn test_new() {
        let x = 12;
        let y = 14;
        let default = 'a';

        let td = Table2d::new(x, y, default);
        for i in 0..x {
            for j in 0..y {
                assert_eq!(td.get(i, j), default);
            }
        }
    }

    #[test]
    #[should_panic]
    fn test_out_of_bounds() {
        let x = 12;
        let y = 14;
        let default = 'a';

        let td = Table2d::new(x, y, default);
        td.get(x, y);
    }

    #[test]
    fn test_set() {
        let x = 10;
        let y = 2;
        let default = '$';

        let mut td = Table2d::new(x, y, default);
        let mut i = 0;
        for c in 'a'..'j' {
            td.set(i, 0, c);
            assert_eq!(td.get(i, 0), c);
            i += 1;
        }
    }

    #[test]
    fn test_slice() {
        let x = 4;
        let y = 2;
        let default = '$';
        let mut td = Table2d::new(x, y, default);
        let mut i = 0;
        for c in 'a'..='h' {
            let a = i % x;
            let b = i / x;
            td.set(a, b, c);
            i += 1;
        }

        assert_eq!(td.to_slice(), ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']);
    }
}
