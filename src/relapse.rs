use crate::table2d::Table2d;
use raylib::prelude::Color;

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct Vector2i32 {
    pub x: i32,
    pub y: i32,
}
pub const BOARD_SIZE: Vector2i32 = Vector2i32 { x: 14, y: 10 };

#[derive(Debug, Clone, Copy)]
pub struct Piece {
    pub position: Vector2i32,
    pub shape: Shape,
    pub player: Player,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Player {
    One(raylib::color::Color),
    Two(raylib::color::Color),
}

#[derive(Debug, Clone, Copy)]
pub enum Shape {
    Square,
    Circle,
    TriangleLeft,
    TriangleRight,
}

#[derive(Debug, Clone, Copy)]
pub enum Tile {
    Home(Player, Shape),
    Entry(Player),
    Finish(Player),
    NoDiagonal,
    Blank,
}

pub struct Board {
    pub dimensions: Vector2i32,
    pub tile_selected: Option<Vector2i32>,
    pub tiles_move: Vec<Vector2i32>,
    pub pieces: [Piece; 12],
    pub tiles: Table2d<Tile>,
    pub curent_turn: Player,
}

impl Board {
    pub fn new(dimensions: Vector2i32) -> Board {
        let default_pieces = [
            Piece {
                position: Vector2i32 { x: 0, y: 2 },
                player: Player::One(raylib::color::Color::YELLOW),
                shape: Shape::Square,
            },
            Piece {
                position: Vector2i32 { x: 0, y: 3 },
                player: Player::One(raylib::color::Color::YELLOW),
                shape: Shape::TriangleLeft,
            },
            Piece {
                position: Vector2i32 { x: 0, y: 4 },
                player: Player::One(raylib::color::Color::YELLOW),
                shape: Shape::Circle,
            },
            Piece {
                position: Vector2i32 { x: 0, y: 5 },
                player: Player::One(raylib::color::Color::YELLOW),
                shape: Shape::Circle,
            },
            Piece {
                position: Vector2i32 { x: 0, y: 6 },
                player: Player::One(raylib::color::Color::YELLOW),
                shape: Shape::TriangleLeft,
            },
            Piece {
                position: Vector2i32 { x: 0, y: 7 },
                player: Player::One(raylib::color::Color::YELLOW),
                shape: Shape::Square,
            },
            Piece {
                position: Vector2i32 { x: 13, y: 2 },
                player: Player::Two(raylib::color::Color::RED),
                shape: Shape::Square,
            },
            Piece {
                position: Vector2i32 { x: 13, y: 3 },
                player: Player::Two(raylib::color::Color::RED),
                shape: Shape::TriangleRight,
            },
            Piece {
                position: Vector2i32 { x: 13, y: 4 },
                player: Player::Two(raylib::color::Color::RED),
                shape: Shape::Circle,
            },
            Piece {
                position: Vector2i32 { x: 13, y: 5 },
                player: Player::Two(raylib::color::Color::RED),
                shape: Shape::Circle,
            },
            Piece {
                position: Vector2i32 { x: 13, y: 6 },
                player: Player::Two(raylib::color::Color::RED),
                shape: Shape::TriangleRight,
            },
            Piece {
                position: Vector2i32 { x: 13, y: 7 },
                player: Player::Two(raylib::color::Color::RED),
                shape: Shape::Square,
            },
        ];

        let mut tiles = Table2d::new(14, 10, Tile::Blank);

        // default tiles:
        // Home row orange
        tiles.set(0, 2, Tile::Home(Player::One(Color::ORANGE), Shape::Square));
        tiles.set(
            0,
            3,
            Tile::Home(Player::One(Color::ORANGE), Shape::TriangleLeft),
        );
        tiles.set(0, 4, Tile::Home(Player::One(Color::ORANGE), Shape::Circle));
        tiles.set(0, 5, Tile::Home(Player::One(Color::ORANGE), Shape::Circle));
        tiles.set(
            0,
            6,
            Tile::Home(Player::One(Color::ORANGE), Shape::TriangleLeft),
        );
        tiles.set(0, 7, Tile::Home(Player::One(Color::ORANGE), Shape::Square));
        // No diagonal
        tiles.set(4,3, Tile::NoDiagonal);
        tiles.set(3,4, Tile::NoDiagonal);
        tiles.set(3, 5, Tile::NoDiagonal);
        tiles.set(4, 6, Tile::NoDiagonal);

        tiles.set(9, 3, Tile::NoDiagonal);
        tiles.set(10, 4, Tile::NoDiagonal);
        tiles.set(10, 5, Tile::NoDiagonal);
        tiles.set(9, 6, Tile::NoDiagonal);
        // Middle
        tiles.set(4, 4, Tile::Entry(Player::One(Color::MAROON)));
        tiles.set(4, 5, Tile::Entry(Player::One(Color::MAROON)));
        tiles.set(5, 4, Tile::Finish(Player::One(Color::DARKGREEN)));
        tiles.set(5, 5, Tile::Finish(Player::One(Color::DARKGREEN)));
        tiles.set(6, 4, Tile::Finish(Player::One(Color::DARKGREEN)));
        tiles.set(6, 5, Tile::Finish(Player::One(Color::DARKGREEN)));

        tiles.set(7, 4, Tile::Finish(Player::Two(Color::GREEN)));
        tiles.set(7, 5, Tile::Finish(Player::Two(Color::GREEN)));
        tiles.set(8, 4, Tile::Finish(Player::Two(Color::GREEN)));
        tiles.set(8, 5, Tile::Finish(Player::Two(Color::GREEN)));
        tiles.set(9, 4, Tile::Entry(Player::Two(Color::ORANGE)));
        tiles.set(9, 5, Tile::Entry(Player::Two(Color::ORANGE)));

        // Home row red
        tiles.set(13, 2, Tile::Home(Player::Two(Color::MAROON), Shape::Square));
        tiles.set(
            13,
            3,
            Tile::Home(Player::One(Color::MAROON), Shape::TriangleRight),
        );
        tiles.set(13, 4, Tile::Home(Player::Two(Color::MAROON), Shape::Circle));
        tiles.set(13, 5, Tile::Home(Player::Two(Color::MAROON), Shape::Circle));
        tiles.set(
            13,
            6,
            Tile::Home(Player::Two(Color::MAROON), Shape::TriangleRight),
        );
        tiles.set(13, 7, Tile::Home(Player::Two(Color::MAROON), Shape::Square));
        let tiles_move = Vec::new();
        Board {
            dimensions,
            tile_selected: None,
            pieces: default_pieces,
            tiles_move,
            tiles,
            curent_turn: Player::One(Color::YELLOW)
        }
    }

    pub fn move_piece(&mut self, from: Vector2i32, to: Vector2i32) {
        for mut piece in self.pieces.iter_mut() {
            if piece.position == from {
                piece.position = to;
                break;
            }
        }
    }

    pub fn valid_tile(&self, tile: Vector2i32) -> bool {
        (0..self.dimensions.x).contains(&tile.x) && (0..self.dimensions.y).contains(&tile.y)
    }
}
