mod control;
mod draw;
mod relapse;
mod table2d;
mod utils;

use draw::viewport::*;
use raylib::prelude::*;

fn main() {
    const WORLD_DIMENSIONS: relapse::Vector2i32 = relapse::Vector2i32 { x: 14, y: 10 };
    const INITIAL_WINDOW_DIMENSIONS: relapse::Vector2i32 = relapse::Vector2i32 { x: 640, y: 480 };

    let (mut rl, thread) = raylib::init()
        .size(
            INITIAL_WINDOW_DIMENSIONS.x as i32,
            INITIAL_WINDOW_DIMENSIONS.y as i32,
        )
        .title("Hello, World")
        .resizable()
        .build();
    rl.set_target_fps(30);

    let mut board = relapse::Board::new(WORLD_DIMENSIONS);
    let mut viewport = Viewport::new(INITIAL_WINDOW_DIMENSIONS, WORLD_DIMENSIONS);

    while !rl.window_should_close() {
        control::mouse_input_board(&rl, viewport.camera, &mut board);
        if rl.is_window_resized() {
            viewport.on_resize(relapse::Vector2i32 {
                x: rl.get_screen_width(),
                y: rl.get_screen_height(),
            });
        }

        let mut d = rl.begin_drawing(&thread);
        d.clear_background(match board.curent_turn {
            relapse::Player::One(c) => c,
            relapse::Player::Two(c) => c,
        });
        let d2 = d.begin_mode2D(viewport.camera);

        draw::draw_board(d2, &board);
    }
}
