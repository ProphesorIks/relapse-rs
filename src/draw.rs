use crate::relapse::*;
use crate::utils;
use raylib::{ffi::Rectangle, prelude::*};

pub fn draw_board(mut d: RaylibMode2D<RaylibDrawHandle>, board: &Board) {
    for i in 0..(BOARD_SIZE.x) {
        for j in 0..(BOARD_SIZE.y) {
            let rectangle = Rectangle {
                x: (i as f32),
                y: (j as f32),
                width: 1.0,
                height: 1.0,
            };

            let tile = board.tiles.get(i as u32, j as u32);

            d.draw_rectangle_rec(rectangle, {
                let p = match tile {
                    Tile::Entry(p) => match p {
                        p => Some(p),
                    },
                    Tile::Finish(p) => match p {
                        p => Some(p),
                    },
                    Tile::Home(p, _) => match p {
                        p => Some(p),
                    },
                    Tile::Blank | Tile::NoDiagonal => None,
                };
                match p {
                    Some(p) => match p {
                        Player::One(c) => c,
                        Player::Two(c) => c,
                    },
                    None => Color::GRAY,
                }
            });

            if let Tile::Home(_, shape) = tile {
                draw_shape(
                    &mut d,
                    Vector2 {
                        x: rectangle.x + rectangle.width / 2.0,
                        y: rectangle.y + rectangle.height / 2.0,
                    },
                    Color::BLACK,
                    shape,
                );
            }
        }
    }

    d.draw_line_ex(
        Vector2 { x: 0.0, y: 0.0 },
        Vector2 { x: 4.0, y: 4.0 },
        0.1,
        Color::BLACK,
    );
    d.draw_line_ex(
        Vector2 { x: 0.0, y: 10.0 },
        Vector2 { x: 4.0, y: 6.0 },
        0.1,
        Color::BLACK,
    );
    d.draw_line_ex(
        Vector2 { x: 14.0, y: 0.0 },
        Vector2 { x: 10.0, y: 4.0 },
        0.1,
        Color::BLACK,
    );
    d.draw_line_ex(
        Vector2 { x: 14.0, y: 10.0 },
        Vector2 { x: 10.0, y: 6.0 },
        0.1,
        Color::BLACK,
    );
    d.draw_circle_v(Vector2 { x: 4.0, y: 4.0 }, 0.25, Color::BLACK);
    d.draw_circle_v(Vector2 { x: 4.0, y: 6.0 }, 0.25, Color::BLACK);
    d.draw_circle_v(Vector2 { x: 10.0, y: 4.0 }, 0.25, Color::BLACK);
    d.draw_circle_v(Vector2 { x: 10.0, y: 6.0 }, 0.25, Color::BLACK);

    d.draw_circle_v(Vector2 { x: 6.0, y: 5.0 }, 0.35, Color::MAROON);
    d.draw_circle_v(Vector2 { x: 8.0, y: 5.0 }, 0.35, Color::ORANGE);
    // draw grid
    for i in 0..(BOARD_SIZE.x + 1) {
        d.draw_line_ex(
            Vector2 {
                x: i as f32,
                y: 0.0,
            },
            Vector2 {
                x: i as f32,
                y: 10.00,
            },
            0.1,
            Color::BLACK,
        );
    }

    for i in 0..(BOARD_SIZE.y + 1) {
        d.draw_line_ex(
            Vector2 {
                x: 0.0,
                y: i as f32,
            },
            Vector2 {
                x: 14.0,
                y: i as f32,
            },
            0.1,
            Color::BLACK,
        );
    }

    for piece in board.pieces {
        let pos = Vector2 {
            x: (piece.position.x as f32 + 0.5),
            y: (piece.position.y as f32 + 0.5),
        };

        draw_shape(&mut d, pos, player_color(piece.player), piece.shape);
    }
    
    match board.tile_selected {
        Some(t) => {
            utils::draw_rectangle_lines_ex(
                &mut d,
                Rectangle{
                    x: t.x as f32,
                    y: t.y as f32,
                    width: 1.0,
                    height:1.0 
                },
                0.125,
                Color::LIME,
            );
        }
        _ => (),
    }

    for tile in &board.tiles_move{
        utils::draw_rectangle_lines_ex(
            &mut d,
            Rectangle {
               x: tile.x as f32,
               y: tile.y as f32,
                width: 1.0,
                height:1.0 
            },
            0.125, 
            Color::PINK,
        );
    }
}

fn draw_shape(
    mut d: &mut RaylibMode2D<RaylibDrawHandle>,
    origin: Vector2,
    color: Color,
    shape: Shape,
) {
    match shape {
        Shape::Circle => {
            let radius = 0.32;
            d.draw_circle_v(origin, radius, color);
        }
        Shape::Square => {
            let size = 0.6;
            d.draw_rectangle_rec(
                Rectangle {
                    x: origin.x - size / 2.0,
                    y: origin.y - size / 2.0,
                    width: size,
                    height: size,
                },
                color,
            )
        }
        Shape::TriangleLeft => draw_triangle_simple(&mut d, origin, 0.35, (PI * 2.0) as f32, color),
        Shape::TriangleRight => draw_triangle_simple(&mut d, origin, 0.35, PI as f32, color),
    }
}

fn player_color(player: Player) -> Color {
    match player {
        Player::One(c) => c,
        Player::Two(c) => c,
    }
}

fn draw_triangle_simple(
    d: &mut RaylibMode2D<RaylibDrawHandle>,
    origin: Vector2,
    size: f32,
    dir: f32,
    color: Color,
) {
    let v1 = Vector2 {
        x: origin.x + (size * dir.cos()),
        y: origin.y + (size * dir.sin()),
    };

    let v3 = Vector2 {
        x: origin.x + (size * (dir + PI as f32 * (2.0 / 3.0)).cos()),
        y: origin.y + (size * (dir + PI as f32 * (2.0 / 3.0)).sin()),
    };
    let v2 = Vector2 {
        x: origin.x + (size * (dir + PI as f32 * (4.0 / 3.0)).cos()),
        y: origin.y + (size * (dir + PI as f32 * (4.0 / 3.0)).sin()),
    };

    d.draw_triangle(v1, v2, v3, color);
}

pub mod viewport {
    use crate::relapse::*;
    use raylib::prelude::*;

    pub struct Viewport {
        pub camera: Camera2D,
        pub screen_dimensions: Vector2i32,
        world_dimensions: Vector2i32,
    }

    impl Viewport {
        pub fn new(screen_dimensions: Vector2i32, world_dimensions: Vector2i32) -> Viewport {
            let mut vp = Viewport {
                camera: Camera2D {
                    offset: Vector2 { x: 0.0, y: 0.0 },
                    target: Vector2 { x: 0.0, y: 0.0 },
                    zoom: 1.0,
                    rotation: 0.0,
                },
                screen_dimensions,
                world_dimensions,
            };
            vp.on_resize(vp.screen_dimensions);
            vp
        }
        pub fn on_resize(&mut self, screen_dimensions: Vector2i32) {
            self.screen_dimensions = screen_dimensions;
            let aspect_world = self.world_dimensions.x as f32 / self.world_dimensions.y as f32;
            let aspect_screen = self.screen_dimensions.x as f32 / self.screen_dimensions.y as f32;
            if aspect_screen > aspect_world {
                self.camera.zoom = self.screen_dimensions.y as f32 / self.world_dimensions.y as f32;
            } else {
                self.camera.zoom = self.screen_dimensions.x as f32 / self.world_dimensions.x as f32;
            }

            self.camera.target = Vector2 {
                x: self.world_dimensions.x as f32 / 2.0,
                y: self.world_dimensions.y as f32 / 2.0,
            };
            self.camera.offset = Vector2 {
                x: self.screen_dimensions.x as f32 / 2.0,
                y: self.screen_dimensions.y as f32 / 2.0,
            };
        }
    }
}
