use raylib::prelude::*;

pub fn draw_rectangle_lines_ex(d: &mut RaylibMode2D<RaylibDrawHandle>, rec: impl Into<ffi::Rectangle>, thick: f32, color: impl Into<ffi::Color>) {
    let rec = rec.into();
    let color = color.into();
    let top = Rectangle  {
        x: rec.x,
        y: rec.y,
        width: rec.width,
        height: thick
    };
    let bottom = Rectangle  {
        x: rec.x,
        y: rec.y - thick + rec.height,
        width: rec.width,
        height: thick
    };

    let left = Rectangle {
        x: rec.x,
        y: rec.y + thick,
        width: thick,
        height: rec.height - thick * 2.0,
    };

    let right = Rectangle {
        x: rec.x - thick + rec.width,
        y: rec.y + thick,
        width: thick,
        height: rec.height - thick * 2.0,
    };
    d.draw_rectangle_rec(top, color);
    d.draw_rectangle_rec(bottom, color);
    d.draw_rectangle_rec(left, color);
    d.draw_rectangle_rec(right, color);
}