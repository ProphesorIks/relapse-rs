use raylib::{prelude, RaylibHandle};
use crate::relapse;

fn get_mouse_tile(
    rl: &RaylibHandle,
    cam: prelude::Camera2D,
    board: &relapse::Board,
) -> relapse::Vector2i32 {
    let coords = rl.get_screen_to_world2D(rl.get_mouse_position(), cam);
    relapse::Vector2i32 {
        x: (coords.x as i32).clamp(0, board.dimensions.x - 1),
        y: (coords.y as i32).clamp(0, board.dimensions.y -1),
    }
}

pub fn mouse_input_board(rl: &RaylibHandle, cam: prelude::Camera2D, board: &mut relapse::Board) {
    let tile_coords = get_mouse_tile(rl, cam, board);

    if rl.is_mouse_button_pressed(prelude::MouseButton::MOUSE_LEFT_BUTTON) {
        board.tile_selected = match board.tile_selected {
            Some(selected) => {
                if selected == tile_coords {
                    None
                } else {
                    Some(tile_coords)
                }
            }
            None => {
                Some(tile_coords)
            }
        };

        set_move_tiles(board);
    }

    if rl.is_mouse_button_pressed(prelude::MouseButton::MOUSE_RIGHT_BUTTON) {
        match board.tile_selected {
            Some(selected_tile ) => {
                let selected_piece = board.pieces.iter().find(|pc| pc.position == selected_tile && std::mem::discriminant(&pc.player) == std::mem::discriminant(&board.curent_turn));
                match selected_piece {
                    Some(piece) => {
                        if board.tiles_move.iter().find(|tl| **tl == tile_coords).is_some() {
                            board.move_piece(piece.position, tile_coords);
                            board.curent_turn = match board.curent_turn {
                                relapse::Player::One(_) => relapse::Player::Two(prelude::Color::RED),
                                relapse::Player::Two(_) => relapse::Player::One(prelude::Color::YELLOW),
                            };
                            board.tiles_move.clear();
                        }
                    }
                    None => {}
                }
            }
            None => {}
        }
    }
}

fn set_move_tiles(board: &mut relapse::Board) {
    board.tiles_move.clear();
    let mut selected_piece = None;
    for piece in board.pieces {
        match board.tile_selected {
            Some(tile) => {
                if piece.position == tile {
                    selected_piece = Some(piece);
                }
            },
            None => {
                break
            }
        }
    };

    match selected_piece {
        Some(piece) => {
            let mut tiles = Vec::new();
            if std::mem::discriminant(&piece.player) != std::mem::discriminant(&board.curent_turn) {
                return;
            }
            match piece.shape {
                relapse::Shape::Square => {
                    tiles.append(&mut vec![relapse::Vector2i32{
                            x: - 1,
                            y: 0,
                        },
                        relapse::Vector2i32{
                            x: 1,
                            y: 0,
                        },
                        relapse::Vector2i32{
                            x: 0,
                            y: -1,
                        },
                        relapse::Vector2i32{
                            x: 0,
                            y: 1,
                        }
                    ]);
                },
                relapse::Shape::TriangleLeft | relapse::Shape::TriangleRight => {
                    tiles.append(&mut vec![
                        relapse::Vector2i32{
                            x: - 1,
                            y: - 1,
                        },
                        relapse::Vector2i32{
                            x: 1,
                            y: - 1,
                        },
                        relapse::Vector2i32{
                            x: - 1,
                            y: 1,
                        },
                        relapse::Vector2i32{
                            x: 1,
                            y: 1,
                        }
                    ])
                },
                relapse::Shape::Circle => {
                    tiles.append(&mut vec![relapse::Vector2i32{
                            x: - 1,
                            y: 0,
                        },
                        relapse::Vector2i32{
                            x: 1,
                            y: 0,
                        },
                        relapse::Vector2i32{
                            x: 0,
                            y: -1,
                        },
                        relapse::Vector2i32{
                            x: 0,
                            y: 1,
                        },
                        relapse::Vector2i32{
                            x: - 1,
                            y: - 1,
                        },
                        relapse::Vector2i32{
                            x: 1,
                            y: - 1,
                        },
                        relapse::Vector2i32{
                            x: - 1,
                            y: 1,
                        },
                        relapse::Vector2i32{
                            x: 1,
                            y: 1,
                        }
                    ]);
                }
            }

            for tile in tiles {
                let absolute_tile = relapse::Vector2i32{x: piece.position.x + tile.x, y: piece.position.y + tile.y};
                if board.valid_tile(absolute_tile) {
                    if board.pieces.iter().find(|p| p.position == absolute_tile && std::mem::discriminant(&p.player) == std::mem::discriminant(&piece.player)).is_some() {
                        continue;
                    }
                    let piece_tile = board.tiles.get(piece.position.x as u32, piece.position.y as u32);
                    let move_tile = board.tiles.get(absolute_tile.x as u32, absolute_tile.y as u32);
                    match move_tile{
                        relapse::Tile::Home(_, _) => {
                            continue;
                        },
                        relapse::Tile::NoDiagonal => {
                            match piece_tile {
                                relapse::Tile::NoDiagonal => {
                                    if tile.x != 0 && tile.y != 0 {
                                        continue;
                                    }
                                },
                                _ => {}
                            }
                        },
                        relapse::Tile::Finish(p) => {
                            if std::mem::discriminant(&p) == std::mem::discriminant(&piece.player) {
                                continue;
                            }
                            match piece_tile {
                                relapse::Tile::Entry(_) | relapse::Tile::Finish(_) => {}
                                _ => {continue;}
                            }
                        },
                        relapse::Tile::Entry(p) => {
                            if std::mem::discriminant(&p) == std::mem::discriminant(&piece.player) {
                                continue;
                            }
                            match p {
                               relapse::Player::One(_) => {
                                 if tile.x != 1 {
                                    continue;
                                 }

                               },
                               relapse::Player::Two(_) => {
                                if tile.x != -1 {
                                    continue;
                                }
                               }
                            }

                            match piece_tile {
                                relapse::Tile::Blank => {continue;}
                                _ => {}
                            }
                        }
                        _ => {}
                    }

                    match piece_tile {
                        relapse::Tile::Entry(_) => {
                            match move_tile {
                                relapse::Tile::Finish(_) => {}
                                _ => {continue;}
                            }
                        }
                        relapse::Tile::Finish(_) => {
                            match move_tile {
                                relapse::Tile::Finish(_) => {
                                }
                               _ => {continue;} 
                            }
                        }
                        _ => {}
                    }
                }else{
                    continue;
                }

                board.tiles_move.push(absolute_tile);
            }

        },
        _ => {}    
    };
    
}
